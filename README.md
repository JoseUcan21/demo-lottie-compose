# Demo Lottie Compose

En este video aprenderás a como integrar una animación Lottie en JetPackCompose
Esto te permitirá dar mejor visualización a tus aplicaciones.

Página de los lotties
[Click aquí](https://lottiefiles.com/)

Documentación
[Click aquí](http://airbnb.io/lottie/#/android-compose)
